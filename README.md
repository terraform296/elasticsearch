# Elasticsearch Installation Steps

### Elasticsearch Installation Steps:

- [ ] Update System
```
sudo yum -y update
sudo reboot
```
- [ ] Install Java / OpenJDK
```
sudo yum -y install vim java-11-openjdk java-11-openjdk-devel
```
- [ ] Add ElasticSearch Yum repository
```
sudo tee /etc/yum.repos.d/elasticsearch.repo
[elasticsearch-7.x]
name=Elasticsearch repository for 7.x packages
baseurl=https://artifacts.elastic.co/packages/oss-7.x/yum
gpgcheck=1
gpgkey=https://artifacts.elastic.co/GPG-KEY-elasticsearch
enabled=1
autorefresh=1
type=rpm-md
EOF
```
- [ ] Install Elasticsearch
```
sudo yum -y install elasticsearch-oss
```

- [ ] Configure Java memory Limits
```
/etc/elasticsearch/jvm.options
-Xms1g
-Xmx1g

-Xms256m
-Xmx512m
```

- [ ] Restart Elasticsearch Service 
```
sudo systemctl enable --now elasticsearch
```


- [ ] Example Configuration File
```
cluster.name: ElasticSearch Cluster
node.name: "ESNode1"
network.host: 192.168.0.1
http.port: 9200
bootstrap.memory_lock: true
xpack.security.enabled: false
discovery.seed_hosts: ["192.168.0.1"]
indices.query.bool.max_clause_count: 9024
```


#### KIBNA Installation Steps
- [ ]  Use the below to install kibana
```
sudo yum install kibana-oss logstash
```
- [ ] Start the Kibana service 
```
systemctl enable --now kibana
```
- [ ] Kibana Example configuration File
```
 /etc/kibana/kibana.yml
server.host: "0.0.0.0"
server.name: "kibana.example.com"
elasticsearch.url: "http://localhost:9200"
```




#### Agents
##### Beats
- [ ] Link
```
https://www.elastic.co/beats/
```
